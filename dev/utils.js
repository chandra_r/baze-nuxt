export function parseDateStr(date) {
	const getDate = new Date(date)
	return getDate.toDateString()
}
