import GetApiClientServices from '@/services/GetApiClientServices.js'
export const state = () => ({
	ourService: {}
})
export const mutations = {
	SET_SERVICES(state, ourService) {
		state.ourService = ourService
	}
}
export const actions = {
	fetchServices({ commit }) {
		return GetApiClientServices.getOurServices().then(response => {
			commit('SET_SERVICES', response.data)
		})
	}
}
