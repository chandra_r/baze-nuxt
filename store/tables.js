import TableService from '@/services/TableService.js'
export const state = () => ({
	tables: []
})
export const mutations = {
	SET_TABLE(state, tables) {
		state.tables = tables
	}
}
export const actions = {
	fetchTables({ commit }) {
		return TableService.getTables().then(response => {
			commit('SET_TABLE', response.data)
		})
	}
}
