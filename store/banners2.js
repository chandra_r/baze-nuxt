import BannersService from '@/services/BannersService.js'
export const state = () => ({
	banners: []
})
export const mutations = {
	SET_MAINBANNER(state, banners) {
		state.banners = banners
	}
}
export const actions = {
	fetchMainBanner({ commit }) {
		return BannersService.getMainBanner().then(response => {
			// console.log(response.data)
			commit('SET_MAINBANNER', response.data)
		})
	}
}
