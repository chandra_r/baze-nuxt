import MainNavService from '@/services/MainNavService.js'
export const state = () => ({
	menus: []
})
export const mutations = {
	SET_MENUS(state, menus) {
		state.menus = menus
	}
}
export const actions = {
	fetchMenus({ commit }) {
		return MainNavService.getMenus().then(response => {
			commit('SET_MENUS', response.data)
		})
	}
}
