import NewsService from '@/services/NewsService.js'
export const state = () => ({
	articles: []
})
export const mutations = {
	SET_ARTICLES(state, articles) {
		state.articles = articles
	}
}
export const actions = {
	fetchArticles({ commit }) {
		return NewsService.getNews().then(response => {
			commit('SET_ARTICLES', response.data)
		})
	}
}
