// import Vue from 'vue'
// import MainNav from '~/components/header/MainNav.vue'

// Vue.component('MainNav', MainNav)
console.log('main nav')
import { mapState } from 'vuex'
export default {
	async fetch({ store, error }) {
		try {
			await store.dispatch('menus/fetchMenus')
			console.log(store)
		} catch (e) {
			error({
				statusCode: 503,
				message: 'Unable'
			})
		}
	},
	computed: mapState({
		menus: state => state.menus.menus
	})
}
