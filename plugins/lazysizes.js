/* eslint-disable no-var */
;(function(window, factory) {
	const lazySizes = factory(window, window.document, Date)
	window.lazySizes = lazySizes
	if (typeof module === 'object' && module.exports) {
		module.exports = lazySizes
	}
})(typeof window !== 'undefined' ? window : {}, function l(
	window,
	document,
	Date
) {
	// Pass in the windoe Date function also for SSR because the Date class can be lost
	'use strict'
	/* jshint eqnull:true */

	// eslint-disable-next-line prefer-const
	let lazysizes, lazySizesCfg
	;(function() {
		let prop

		const lazySizesDefaults = {
			lazyClass: 'lazyload',
			loadedClass: 'lazyloaded',
			loadingClass: 'lazyloading',
			preloadClass: 'lazypreload',
			errorClass: 'lazyerror',
			// strictClass: 'lazystrict',
			autosizesClass: 'lazyautosizes',
			srcAttr: 'data-src',
			srcsetAttr: 'data-srcset',
			sizesAttr: 'data-sizes',
			// preloadAfterLoad: false,
			minSize: 40,
			customMedia: {},
			init: true,
			expFactor: 1.5,
			hFac: 0.8,
			loadMode: 2,
			loadHidden: true,
			ricTimeout: 0,
			throttleDelay: 125
		}

		lazySizesCfg = window.lazySizesConfig || window.lazysizesConfig || {}

		for (prop in lazySizesDefaults) {
			if (!(prop in lazySizesCfg)) {
				lazySizesCfg[prop] = lazySizesDefaults[prop]
			}
		}
	})()

	if (!document || !document.getElementsByClassName) {
		return {
			init() {},
			cfg: lazySizesCfg,
			noSupport: true
		}
	}

	const docElem = document.documentElement

	const supportPicture = window.HTMLPictureElement

	const _addEventListener = 'addEventListener'

	const _getAttribute = 'getAttribute'

	/**
	 * Update to bind to window because 'this' becomes null during SSR
	 * builds.
	 */
	const addEventListener = window[_addEventListener].bind(window)

	const setTimeout = window.setTimeout

	const requestAnimationFrame = window.requestAnimationFrame || setTimeout

	const requestIdleCallback = window.requestIdleCallback

	const regPicture = /^picture$/i

	const loadEvents = ['load', 'error', 'lazyincluded', '_lazyloaded']

	const regClassCache = {}

	const forEach = Array.prototype.forEach

	const hasClass = function(ele, cls) {
		if (!regClassCache[cls]) {
			regClassCache[cls] = new RegExp('(\\s|^)' + cls + '(\\s|$)')
		}
		return (
			regClassCache[cls].test(ele[_getAttribute]('class') || '') &&
			regClassCache[cls]
		)
	}

	const addClass = function(ele, cls) {
		if (!hasClass(ele, cls)) {
			ele.setAttribute(
				'class',
				(ele[_getAttribute]('class') || '').trim() + ' ' + cls
			)
		}
	}

	const removeClass = function(ele, cls) {
		let reg
		if ((reg = hasClass(ele, cls))) {
			ele.setAttribute(
				'class',
				(ele[_getAttribute]('class') || '').replace(reg, ' ')
			)
		}
	}

	// eslint-disable-next-line no-var
	var addRemoveLoadEvents = function(dom, fn, add) {
		const action = add ? _addEventListener : 'removeEventListener'
		if (add) {
			addRemoveLoadEvents(dom, fn)
		}
		loadEvents.forEach(function(evt) {
			dom[action](evt, fn)
		})
	}

	const triggerEvent = function(elem, name, detail, noBubbles, noCancelable) {
		const event = document.createEvent('Event')

		if (!detail) {
			detail = {}
		}

		detail.instance = lazysizes

		event.initEvent(name, !noBubbles, !noCancelable)

		event.detail = detail

		elem.dispatchEvent(event)
		return event
	}

	const updatePolyfill = function(el, full) {
		let polyfill
		if (!supportPicture && (polyfill = window.picturefill || lazySizesCfg.pf)) {
			if (full && full.src && !el[_getAttribute]('srcset')) {
				el.setAttribute('srcset', full.src)
			}
			polyfill({ reevaluate: true, elements: [el] })
		} else if (full && full.src) {
			el.src = full.src
		}
	}

	const getCSS = function(elem, style) {
		return (getComputedStyle(elem, null) || {})[style]
	}

	const getWidth = function(elem, parent, width) {
		width = width || elem.offsetWidth

		while (width < lazySizesCfg.minSize && parent && !elem._lazysizesWidth) {
			width = parent.offsetWidth
			parent = parent.parentNode
		}

		return width
	}

	const rAF = (function() {
		let running, waiting
		const firstFns = []
		const secondFns = []
		let fns = firstFns

		const run = function() {
			const runFns = fns

			fns = firstFns.length ? secondFns : firstFns

			running = true
			waiting = false

			while (runFns.length) {
				runFns.shift()()
			}

			running = false
		}

		const rafBatch = function(fn, queue) {
			if (running && !queue) {
				fn.apply(this, arguments)
			} else {
				fns.push(fn)

				if (!waiting) {
					waiting = true
					;(document.hidden ? setTimeout : requestAnimationFrame)(run)
				}
			}
		}

		rafBatch._lsFlush = run

		return rafBatch
	})()

	const rAFIt = function(fn, simple) {
		return simple
			? function() {
					rAF(fn)
			  }
			: function() {
					const that = this
					const args = arguments
					rAF(function() {
						fn.apply(that, args)
					})
			  }
	}

	const throttle = function(fn) {
		let running
		let lastTime = 0
		const gDelay = lazySizesCfg.throttleDelay
		let rICTimeout = lazySizesCfg.ricTimeout
		const run = function() {
			running = false
			lastTime = Date.now()
			fn()
		}
		const idleCallback =
			requestIdleCallback && rICTimeout > 49
				? function() {
						requestIdleCallback(run, { timeout: rICTimeout })

						if (rICTimeout !== lazySizesCfg.ricTimeout) {
							rICTimeout = lazySizesCfg.ricTimeout
						}
				  }
				: rAFIt(function() {
						setTimeout(run)
				  }, true)
		return function(isPriority) {
			let delay

			if ((isPriority = isPriority === true)) {
				rICTimeout = 33
			}

			if (running) {
				return
			}

			running = true

			delay = gDelay - (Date.now() - lastTime)

			if (delay < 0) {
				delay = 0
			}

			if (isPriority || delay < 9) {
				idleCallback()
			} else {
				setTimeout(idleCallback, delay)
			}
		}
	}

	// based on http://modernjavascript.blogspot.de/2013/08/building-better-debounce.html
	const debounce = function(func) {
		let timeout, timestamp
		const wait = 99
		const run = function() {
			timeout = null
			func()
		}
		// eslint-disable-next-line no-var
		var later = function() {
			const last = Date.now() - timestamp

			if (last < wait) {
				setTimeout(later, wait - last)
			} else {
				;(requestIdleCallback || run)(run)
			}
		}

		return function() {
			timestamp = Date.now()

			if (!timeout) {
				timeout = setTimeout(later, wait)
			}
		}
	}

	const loader = (function() {
		let preloadElems, isCompleted, resetPreloadingTimer, loadMode, started

		let eLvW, elvH, eLtop, eLleft, eLright, eLbottom, isBodyHidden

		const regImg = /^img$/i
		const regIframe = /^iframe$/i

		const supportScroll =
			'onscroll' in window && !/(gle|ing)bot/.test(navigator.userAgent)

		const shrinkExpand = 0
		let currentExpand = 0

		let isLoading = 0
		let lowRuns = -1

		const resetPreloading = function(e) {
			isLoading--
			if (!e || isLoading < 0 || !e.target) {
				isLoading = 0
			}
		}

		const isVisible = function(elem) {
			if (isBodyHidden == null) {
				isBodyHidden = getCSS(document.body, 'visibility') === 'hidden'
			}

			return (
				isBodyHidden ||
				!(
					getCSS(elem.parentNode, 'visibility') === 'hidden' &&
					getCSS(elem, 'visibility') === 'hidden'
				)
			)
		}

		const isNestedVisible = function(elem, elemExpand) {
			let outerRect
			let parent = elem
			let visible = isVisible(elem)

			eLtop -= elemExpand
			eLbottom += elemExpand
			eLleft -= elemExpand
			eLright += elemExpand

			while (
				visible &&
				(parent = parent.offsetParent) &&
				parent !== document.body &&
				parent !== docElem
			) {
				visible = (getCSS(parent, 'opacity') || 1) > 0

				if (visible && getCSS(parent, 'overflow') !== 'visible') {
					outerRect = parent.getBoundingClientRect()
					visible =
						eLright > outerRect.left &&
						eLleft < outerRect.right &&
						eLbottom > outerRect.top - 1 &&
						eLtop < outerRect.bottom + 1
				}
			}

			return visible
		}

		const checkElements = function() {
			let eLlen,
				i,
				rect,
				autoLoadElem,
				loadedSomething,
				elemExpand,
				elemNegativeExpand,
				elemExpandVal,
				beforeExpandVal,
				defaultExpand,
				preloadExpand,
				hFac
			const lazyloadElems = lazysizes.elements

			if (
				(loadMode = lazySizesCfg.loadMode) &&
				isLoading < 8 &&
				(eLlen = lazyloadElems.length)
			) {
				i = 0

				lowRuns++

				for (; i < eLlen; i++) {
					if (!lazyloadElems[i] || lazyloadElems[i]._lazyRace) {
						continue
					}

					if (
						!supportScroll ||
						(lazysizes.prematureUnveil &&
							lazysizes.prematureUnveil(lazyloadElems[i]))
					) {
						unveilElement(lazyloadElems[i])
						continue
					}

					if (
						!(elemExpandVal = lazyloadElems[i][_getAttribute]('data-expand')) ||
						!(elemExpand = elemExpandVal * 1)
					) {
						elemExpand = currentExpand
					}

					if (!defaultExpand) {
						defaultExpand =
							!lazySizesCfg.expand || lazySizesCfg.expand < 1
								? docElem.clientHeight > 500 && docElem.clientWidth > 500
									? 500
									: 370
								: lazySizesCfg.expand

						lazysizes._defEx = defaultExpand

						preloadExpand = defaultExpand * lazySizesCfg.expFactor
						hFac = lazySizesCfg.hFac
						isBodyHidden = null

						if (
							currentExpand < preloadExpand &&
							isLoading < 1 &&
							lowRuns > 2 &&
							loadMode > 2 &&
							!document.hidden
						) {
							currentExpand = preloadExpand
							lowRuns = 0
						} else if (loadMode > 1 && lowRuns > 1 && isLoading < 6) {
							currentExpand = defaultExpand
						} else {
							currentExpand = shrinkExpand
						}
					}

					if (beforeExpandVal !== elemExpand) {
						eLvW = innerWidth + elemExpand * hFac
						elvH = innerHeight + elemExpand
						elemNegativeExpand = elemExpand * -1
						beforeExpandVal = elemExpand
					}

					rect = lazyloadElems[i].getBoundingClientRect()

					if (
						(eLbottom = rect.bottom) >= elemNegativeExpand &&
						(eLtop = rect.top) <= elvH &&
						(eLright = rect.right) >= elemNegativeExpand * hFac &&
						(eLleft = rect.left) <= eLvW &&
						(eLbottom || eLright || eLleft || eLtop) &&
						(lazySizesCfg.loadHidden || isVisible(lazyloadElems[i])) &&
						((isCompleted &&
							isLoading < 3 &&
							!elemExpandVal &&
							(loadMode < 3 || lowRuns < 4)) ||
							isNestedVisible(lazyloadElems[i], elemExpand))
					) {
						unveilElement(lazyloadElems[i])
						loadedSomething = true
						if (isLoading > 9) {
							break
						}
					} else if (
						!loadedSomething &&
						isCompleted &&
						!autoLoadElem &&
						isLoading < 4 &&
						lowRuns < 4 &&
						loadMode > 2 &&
						(preloadElems[0] || lazySizesCfg.preloadAfterLoad) &&
						(preloadElems[0] ||
							(!elemExpandVal &&
								(eLbottom ||
									eLright ||
									eLleft ||
									eLtop ||
									lazyloadElems[i][_getAttribute](lazySizesCfg.sizesAttr) !==
										'auto')))
					) {
						autoLoadElem = preloadElems[0] || lazyloadElems[i]
					}
				}

				if (autoLoadElem && !loadedSomething) {
					unveilElement(autoLoadElem)
				}
			}
		}

		const throttledCheckElements = throttle(checkElements)

		const switchLoadingClass = function(e) {
			const elem = e.target

			if (elem._lazyCache) {
				delete elem._lazyCache
				return
			}

			resetPreloading(e)
			addClass(elem, lazySizesCfg.loadedClass)
			removeClass(elem, lazySizesCfg.loadingClass)
			addRemoveLoadEvents(elem, rafSwitchLoadingClass)
			triggerEvent(elem, 'lazyloaded')
		}
		const rafedSwitchLoadingClass = rAFIt(switchLoadingClass)
		var rafSwitchLoadingClass = function(e) {
			rafedSwitchLoadingClass({ target: e.target })
		}

		const changeIframeSrc = function(elem, src) {
			try {
				elem.contentWindow.location.replace(src)
			} catch (e) {
				elem.src = src
			}
		}

		const handleSources = function(source) {
			let customMedia

			const sourceSrcset = source[_getAttribute](lazySizesCfg.srcsetAttr)

			if (
				(customMedia =
					lazySizesCfg.customMedia[
						source[_getAttribute]('data-media') ||
							source[_getAttribute]('media')
					])
			) {
				source.setAttribute('media', customMedia)
			}

			if (sourceSrcset) {
				source.setAttribute('srcset', sourceSrcset)
			}
		}

		const lazyUnveil = rAFIt(function(elem, detail, isAuto, sizes, isImg) {
			let src, srcset, parent, isPicture, event, firesLoad

			if (
				!(event = triggerEvent(elem, 'lazybeforeunveil', detail))
					.defaultPrevented
			) {
				if (sizes) {
					if (isAuto) {
						addClass(elem, lazySizesCfg.autosizesClass)
					} else {
						elem.setAttribute('sizes', sizes)
					}
				}

				srcset = elem[_getAttribute](lazySizesCfg.srcsetAttr)
				src = elem[_getAttribute](lazySizesCfg.srcAttr)

				if (isImg) {
					parent = elem.parentNode
					isPicture = parent && regPicture.test(parent.nodeName || '')
				}

				firesLoad =
					detail.firesLoad || ('src' in elem && (srcset || src || isPicture))

				event = { target: elem }

				addClass(elem, lazySizesCfg.loadingClass)

				if (firesLoad) {
					clearTimeout(resetPreloadingTimer)
					resetPreloadingTimer = setTimeout(resetPreloading, 2500)
					addRemoveLoadEvents(elem, rafSwitchLoadingClass, true)
				}

				if (isPicture) {
					forEach.call(parent.getElementsByTagName('source'), handleSources)
				}

				if (srcset) {
					elem.setAttribute('srcset', srcset)
				} else if (src && !isPicture) {
					if (regIframe.test(elem.nodeName)) {
						changeIframeSrc(elem, src)
					} else {
						elem.src = src
					}
				}

				if (isImg && (srcset || isPicture)) {
					updatePolyfill(elem, { src })
				}
			}

			if (elem._lazyRace) {
				delete elem._lazyRace
			}
			removeClass(elem, lazySizesCfg.lazyClass)

			rAF(function() {
				// Part of this can be removed as soon as this fix is older: https://bugs.chromium.org/p/chromium/issues/detail?id=7731 (2015)
				const isLoaded = elem.complete && elem.naturalWidth > 1

				if (!firesLoad || isLoaded) {
					if (isLoaded) {
						addClass(elem, 'ls-is-cached')
					}
					switchLoadingClass(event)
					elem._lazyCache = true
					setTimeout(function() {
						if ('_lazyCache' in elem) {
							delete elem._lazyCache
						}
					}, 9)
				}
				if (elem.loading === 'lazy') {
					isLoading--
				}
			}, true)
		})

		var unveilElement = function(elem) {
			if (elem._lazyRace) {
				return
			}
			let detail

			const isImg = regImg.test(elem.nodeName)

			// allow using sizes="auto", but don't use. it's invalid. Use data-sizes="auto" or a valid value for sizes instead (i.e.: sizes="80vw")
			const sizes =
				isImg &&
				(elem[_getAttribute](lazySizesCfg.sizesAttr) ||
					elem[_getAttribute]('sizes'))
			const isAuto = sizes === 'auto'

			if (
				(isAuto || !isCompleted) &&
				isImg &&
				(elem[_getAttribute]('src') || elem.srcset) &&
				!elem.complete &&
				!hasClass(elem, lazySizesCfg.errorClass) &&
				hasClass(elem, lazySizesCfg.lazyClass)
			) {
				return
			}

			// eslint-disable-next-line prefer-const
			detail = triggerEvent(elem, 'lazyunveilread').detail

			if (isAuto) {
				autoSizer.updateElem(elem, true, elem.offsetWidth)
			}

			elem._lazyRace = true
			isLoading++

			lazyUnveil(elem, detail, isAuto, sizes, isImg)
		}

		const afterScroll = debounce(function() {
			lazySizesCfg.loadMode = 3
			throttledCheckElements()
		})

		const altLoadmodeScrollListner = function() {
			if (lazySizesCfg.loadMode === 3) {
				lazySizesCfg.loadMode = 2
			}
			afterScroll()
		}

		var onload = function() {
			if (isCompleted) {
				return
			}
			if (Date.now() - started < 999) {
				setTimeout(onload, 999)
				return
			}

			isCompleted = true

			lazySizesCfg.loadMode = 3

			throttledCheckElements()

			addEventListener('scroll', altLoadmodeScrollListner, true)
		}

		return {
			_() {
				started = Date.now()

				lazysizes.elements = document.getElementsByClassName(
					lazySizesCfg.lazyClass
				)
				preloadElems = document.getElementsByClassName(
					lazySizesCfg.lazyClass + ' ' + lazySizesCfg.preloadClass
				)

				addEventListener('scroll', throttledCheckElements, true)

				addEventListener('resize', throttledCheckElements, true)

				addEventListener('pageshow', function(e) {
					if (e.persisted) {
						const loadingElements = document.querySelectorAll(
							'.' + lazySizesCfg.loadingClass
						)

						if (loadingElements.length && loadingElements.forEach) {
							requestAnimationFrame(function() {
								loadingElements.forEach(function(img) {
									if (img.complete) {
										unveilElement(img)
									}
								})
							})
						}
					}
				})

				if (window.MutationObserver) {
					new MutationObserver(throttledCheckElements).observe(docElem, {
						childList: true,
						subtree: true,
						attributes: true
					})
				} else {
					docElem[_addEventListener](
						'DOMNodeInserted',
						throttledCheckElements,
						true
					)
					docElem[_addEventListener](
						'DOMAttrModified',
						throttledCheckElements,
						true
					)
					setInterval(throttledCheckElements, 999)
				}

				addEventListener('hashchange', throttledCheckElements, true)

				//, 'fullscreenchange'
				;[
					'focus',
					'mouseover',
					'click',
					'load',
					'transitionend',
					'animationend'
				].forEach(function(name) {
					document[_addEventListener](name, throttledCheckElements, true)
				})

				if (/d$|^c/.test(document.readyState)) {
					onload()
				} else {
					addEventListener('load', onload)
					document[_addEventListener](
						'DOMContentLoaded',
						throttledCheckElements
					)
					setTimeout(onload, 20000)
				}

				if (lazysizes.elements.length) {
					checkElements()
					rAF._lsFlush()
				} else {
					throttledCheckElements()
				}
			},
			checkElems: throttledCheckElements,
			unveil: unveilElement,
			_aLSL: altLoadmodeScrollListner
		}
	})()

	var autoSizer = (function() {
		let autosizesElems

		const sizeElement = rAFIt(function(elem, parent, event, width) {
			let sources, i, len
			elem._lazysizesWidth = width
			width += 'px'

			elem.setAttribute('sizes', width)

			if (regPicture.test(parent.nodeName || '')) {
				sources = parent.getElementsByTagName('source')
				for (i = 0, len = sources.length; i < len; i++) {
					sources[i].setAttribute('sizes', width)
				}
			}

			if (!event.detail.dataAttr) {
				updatePolyfill(elem, event.detail)
			}
		})
		const getSizeElement = function(elem, dataAttr, width) {
			let event
			const parent = elem.parentNode

			if (parent) {
				width = getWidth(elem, parent, width)
				event = triggerEvent(elem, 'lazybeforesizes', {
					width,
					dataAttr: !!dataAttr
				})

				if (!event.defaultPrevented) {
					width = event.detail.width

					if (width && width !== elem._lazysizesWidth) {
						sizeElement(elem, parent, event, width)
					}
				}
			}
		}

		const updateElementsSizes = function() {
			let i
			const len = autosizesElems.length
			if (len) {
				i = 0

				for (; i < len; i++) {
					getSizeElement(autosizesElems[i])
				}
			}
		}

		const debouncedUpdateElementsSizes = debounce(updateElementsSizes)

		return {
			_() {
				autosizesElems = document.getElementsByClassName(
					lazySizesCfg.autosizesClass
				)
				addEventListener('resize', debouncedUpdateElementsSizes)
			},
			checkElems: debouncedUpdateElementsSizes,
			updateElem: getSizeElement
		}
	})()

	var init = function() {
		if (!init.i && document.getElementsByClassName) {
			init.i = true
			autoSizer._()
			loader._()
		}
	}

	setTimeout(function() {
		if (lazySizesCfg.init) {
			init()
		}
	})

	lazysizes = {
		cfg: lazySizesCfg,
		autoSizer,
		loader,
		init,
		uP: updatePolyfill,
		aC: addClass,
		rC: removeClass,
		hC: hasClass,
		fire: triggerEvent,
		gW: getWidth,
		rAF
	}

	return lazysizes
})
