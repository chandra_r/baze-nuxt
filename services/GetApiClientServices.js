import axios from 'axios'

const apiClient = axios.create({
	baseURL: `http://localhost:3000`,
	withCredentials: false,
	headers: {
		Accept: 'application/json',
		'Content-Type': 'application/json'
	}
})

export default {
	getOurServices() {
		return apiClient.get('/OurServices')
	},
	getPageComponents() {
		return apiClient.get('/components')
	},
	getHomeComponents() {
		return apiClient.get('/homepage')
	},
	getHomeMainBanner() {
		return apiClient.get('/mainBanner')
	}
}
