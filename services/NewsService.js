import axios from 'axios'

const apiClient = axios.create({
	baseURL: `https://my-json-server.typicode.com/rolexta/bazeDummyData`,
	withCredentials: true,
	headers: {
		Accept: 'application/json',
		'Content-Type': 'application/json'
	}
})

export default {
	getNews() {
		return apiClient.get('/article')
	}
}
