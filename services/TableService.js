import axios from 'axios'

const apiClient = axios.create({
	baseURL: `https://my-json-server.typicode.com/rolexta/bazeDummyData`,
	withCredentials: false,
	headers: {
		Accept: 'application/json',
		'Content-Type': 'application/json'
	}
})

export default {
	getTables() {
		return apiClient.get('/tables')
	}
}
