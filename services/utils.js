/* eslint-disable no-func-assign */
export function filter(cb) {
	return function(arr) {
		return arr.filter(cb)
	}
}

export function equal(comparison) {
	return function(value) {
		return value === comparison
	}
}

export function notEqual(comparison) {
	return function(value) {
		return !equal(comparison)(value)
	}
}

export function parseDateStr(date) {
	const getDate = new Date(date)
	return getDate.toDateString()
}

export function compare(a, b) {
	if (a > b) {
		return 1
	} else if (a < b) {
		return -1
	} else {
		return 0
	}
}

export function isString(a) {
	return typeof a === 'string'
}

export function append(a, bs) {
	return isString(bs) ? bs + a : bs.concat([a])
}

export function prepend(a, bs) {
	return isString(bs) ? a + bs : [a].concat(bs)
}

export function _typeof(obj) {
	if (typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol') {
		_typeof = function(obj) {
			return typeof obj
		}
	} else {
		_typeof = function(obj) {
			return obj &&
				typeof Symbol === 'function' &&
				obj.constructor === Symbol &&
				obj !== Symbol.prototype
				? 'symbol'
				: typeof obj
		}
	}

	return _typeof(obj)
}

export function toList(as, t) {
	return t === 'string' ? as.join('') : as
}

export function sortAsc(as) {
	const bs = Array.from(as.slice(0))

	return toList(bs.sort(compare), _typeof(as))
}

export function sortDsc(as) {
	return sortAsc(as).reverse()
}

export function getMin(as) {
	return sortAsc(as)[0]
}

export function getMax(as) {
	return sortDsc(as)[0]
}

export function add(a, b) {
	return a + b
}

export function sum(arr) {
	return arr.reduce(add)
}

export function empty(as) {
	return as.length === 0
}

export function mempty(as) {
	return isString(as) ? '' : []
}

export function nubBy(f, as) {
	const a = as[0]

	return empty(as)
		? mempty(as)
		: prepend(a, nubBy(f, filter(b => !f(a)(b))(as.slice(1))))
}

export function nub(as) {
	const bs = isString(as) ? Array.from(as) : as

	return toList(nubBy(equal, bs), _typeof(as))
}
