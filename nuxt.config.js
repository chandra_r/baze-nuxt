import webpack from 'webpack'
import EventService from './services/EventService.js'
export default {
	mode: 'universal',
	/*
	 ** Nuxt target
	 ** See https://nuxtjs.org/api/configuration-target
	 */
	target: 'static',
	/*
	 ** Headers of the page
	 */

	head: {
		title: process.env.npm_package_name || '',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{
				hid: 'description',
				name: 'description',
				content: process.env.npm_package_description || ''
			},
			{
				name: 'theme-color',
				content: '#ff6600'
			}
		],
		link: [
			{ rel: 'apple-touch-icon', href: '/apple-icon.png' },
			{ rel: 'icon', type: 'image/png', href: '/favicon.png' }
		]
	},
	/*
	 ** Customize manifest
	 */
	manifest: {
		short_name: 'BzPWA',
		name: 'Baze PWA',
		start_url: './',
		theme_color: '#ff6600',
		background_color: '#ffffff'
	},
	workbox: {
		cacheNames: 'bz_cache'
		// offlinePage: '/offline.html'
	},
	/*
	 ** Customize the progress-bar color
	 */
	loading: { color: '#000' },
	/*
	 ** Global CSS
	 */
	css: [
		'~/assets/scss/main.scss',
		'vue-slick-carousel/dist/vue-slick-carousel.css',
		'swiper/css/swiper.css',
		'@fullcalendar/core/main.css',
		'@fullcalendar/daygrid/main.css',
		'vue-select/dist/vue-select.css'
	],
	/*
	 ** Plugins to load before mounting the App
	 */
	plugins: [
		{ src: '~/plugins/vue-slick-carousel.js' },
		{ src: '~/plugins/lazysizes.js' },
		{ src: '@/plugins/vue-awesome-swiper', mode: 'client' },
		{ src: '~/plugins/full-calendar', mode: 'client' },
		{ src: '~/plugins/v-calendar', mode: 'client' },
		{ src: '~/plugins/vue-select', mode: 'client' },
		{ src: '~/plugins/vue-good-table', mode: 'client' },
		{ src: '~/plugins/bzBtn' },
		{ src: '~/plugins/vuelidate' }
	],
	/*
	 ** Nuxt.js dev-modules
	 */
	buildModules: [
		// Doc: https://github.com/nuxt-community/eslint-module
		'@nuxtjs/eslint-module',
		// Doc: https://github.com/nuxt-community/stylelint-module
		'@nuxtjs/stylelint-module',
		'@nuxtjs/pwa'
	],
	/*
	 ** Nuxt.js modules
	 */
	modules: [
		'@nuxtjs/component-cache',
		'@nuxtjs/axios',
		'@nuxtjs/style-resources',
		'nuxt-highcharts',
		'nuxt-user-agent'
	],
	/*
	 ** Auto import components
	 ** See https://nuxtjs.org/api/configuration-components
	 */
	components: true,
	styleResources: {
		scss: ['./assets/scss/*.scss']
	},
	/*
	 ** Build configuration
	 */
	build: {
		/*
		 ** You can extend webpack config here
		 */
		extend(config, ctx) {},
		postcss: {
			preset: {
				autoprefixer: {
					overrideBrowserslist: ['last 2 versions']
				}
			}
		},
		plugins: [
			new webpack.IgnorePlugin({
				resourceRegExp: /\\@highcharts\/map\\-collection/
			})
		]
	},
	generate: {
		routes: () => {
			return EventService.getEvents().then(response => {
				return response.data.map(event => {
					return '/events/' + event.id
				})
			})
		}
	}
}
